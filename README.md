Pepper&Carrot: Remixes
======================

Tea Party in Komona
-------------------
To render this remix, fetch file `gfx_Pepper-and-Carrot_by-David-Revoy_E16P02.png`, e.g., from [the repository for translations](https://framagit.org/peppercarrot/webcomics/blob/master/ep16_The-Sage-of-the-Mountain/lang/gfx_Pepper-and-Carrot_by-David-Revoy_E16P02.png) or from [the website](https://www.peppercarrot.com/0_sources/ep16_The-Sage-of-the-Mountain/hi-res/gfx-only/gfx_Pepper-and-Carrot_by-David-Revoy_E16P02.jpg) (to use this one, change the picture in the SVG from PNG to JPEG).
